﻿using CMS.Service.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.Interfaces
{
    public interface ICMSUserService : ICMSService
    {
        UserServiceModel GetByLogin(string login);
    }
}
