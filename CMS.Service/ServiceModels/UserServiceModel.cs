﻿using CMS.Common.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace CMS.Service.ServiceModels
{
    public class UserServiceModel
    {
        public long Id { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserRole Role { get; set; }

        public UserStatus Status { get; set; }
    }
}
