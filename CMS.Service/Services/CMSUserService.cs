﻿using CMS.Data.DB;
using CMS.Service.Interfaces;
using CMS.Service.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CMS.Service.Services
{
    public class CMSUserService : ICMSUserService
    {
        private CMSContext _db = new CMSContext();

        public UserServiceModel GetByLogin(string login)
        {
            var user = _db.Users.Where(x => x.Login == login && !x.IsDeleted).SingleOrDefault();

            UserServiceModel result = user!= null ? new UserServiceModel()
            {
                Email = user.Email,
                FirstName = user.FirstName,
                Id = user.Id,
                LastName = user.LastName,
                Login = user.Login,
                Role = user.Role,
                Status = user.Status
            }
            : null;

            return result;
        }
    }
}
