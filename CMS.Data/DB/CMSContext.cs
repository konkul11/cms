﻿using CMS.Data.DB.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB
{
    public class CMSContext : DbContext
    {
        public DbSet<CMSUser> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\sqlexpress;Database=CMSDatabase;Trusted_Connection=True;");
        }
    }
}
