﻿using CMS.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public class CMSUser
    {
        public long Id { get; set; }

        [Column(TypeName = "VARCHAR(50)")]
        [Required]
        public string Login { get; set; }

        [Column(TypeName = "VARCHAR(50)")]
        [Required]
        public string Email { get; set; }

        //tyle znaków bo używam SHA512 do szyfrowania
        [Column(TypeName = "VARCHAR(128)")]
        [Required]
        public string Password { get; set; }

        public Guid Salt { get; set; }

        [StringLength(30)]
        [Required]
        public string FirstName { get; set; }

        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        public UserRole Role { get; set; }

        public bool IsDeleted { get; set; }

        public UserStatus Status { get; set; }

        //public virtual ICollection<CMSMessage> SenderCMSMessages { get; set; }

        //public virtual ICollection<CMSMessage> ReceiverCMSMessages { get; set; }

        //public virtual ICollection<CMSMail> CMSMails { get; set; } // wszystkie maile w ktorych klucz obcy wskazywal na danego usera

        //public virtual ICollection<CMSCowShed> CMSCowSheds { get; set; }

    }
}
