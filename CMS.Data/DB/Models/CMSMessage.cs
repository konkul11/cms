﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public class CMSMessage
    {
        public long Id { get; set; }

        [Required]
        public string Text { get; set; }

        public long SenderCMSUserId { get; set; }

        public long ReceiverCMSUserId { get; set; }

        public DateTime SendDate { get; set; }

        public DateTime? ReadingDate { get; set; }


        public virtual CMSUser SenderCMSUser { get; set; }

        public virtual CMSUser ReceiverCMSUser { get; set; }

    }
}
