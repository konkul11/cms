﻿using CMS.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public abstract class CMSCattle
    {
        public long Id { get; set; }

        [Required]
        [Column("CHAR(14)")]
        public string EarTagNumber { get; set; }

        public bool IsBought { get; set; }

        public decimal? BoughtPrice { get; set; }

        [StringLength(100)]
        public string BoughtPlace { get; set; }

        public long CMSCowShedId { get; set; }

        public DateTime? DeadDate { get; set; }

        public DateTime? DateSold { get; set; }

        public bool IsSold { get; set; }

        public bool IsDeleted { get; set; }

        public CattleType Type { get; set; }

        public long? MotherCMSCowId { get; set; }

        public DateTime? BirthDate { get; set; }

        public CattleSex Sex { get; set; }


        public virtual CMSCowShed CMSCowShed { get; set; }

        public virtual CMSCow MotherCMSCow { get; set; }
    }
}
