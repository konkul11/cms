﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public class CMSCow : CMSCattle
    {
        public int ChildBirthCount { get; set; }

        public bool IsOnAnthibiotics { get; set; }

        public DateTime? AnthibioticsEndDate { get; set; }

        public bool IsPregnant { get; set; }

        public DateTime? ChildBirthDate { get; set; }

        public DateTime? FerilizationDate { get; set; }

        public bool IsMilked { get; set; }
    }
}
