﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public class CMSCowShed
    {
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public short TotalPlacesCount { get; set; }

        public long UserId { get; set; }

        public bool IsDeleted { get; set; }
    }
}
