﻿using CMS.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Data.DB.Models
{
    public class CMSMail
    {
        public long Id { get; set; }

        public MailType Type { get; set; }

        [Required]
        public string Text { get; set; }

        public MailStatusType Status { get; set; }

        public long CMSUserId { get; set; }

        [Column(TypeName = "VARCHAR(50)")]
        [Required]
        public string EmailAdress { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        public DateTime? SendDate { get; set; }

        public DateTime InsertDate { get; set; }

        public virtual CMSUser CMSUser { get; set; }
    }
}
