﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Common.Enums
{
    public enum CattleSex
    {
        Male = 1,
        Female = 2
    }
}
