﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Common.Enums
{
    public enum UserRole
    {
        Administrator = 1,
        User = 2
    }
}
