﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Common.Enums
{
    public enum UserStatus
    {
        Active = 1,
        Inactive = 2
    }
}
