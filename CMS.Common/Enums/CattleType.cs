﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CMS.Common.Enums
{
    public enum CattleType
    {
        Meat = 1,
        Milk = 2
    }
}
