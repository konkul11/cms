﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CMS.Service.Interfaces;
using CMS.Service.ServiceModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Cowshed_Management_System.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly ICMSUserService _cmsUserService;

        public UserController(ICMSUserService cmsUserService)
        {
            _cmsUserService = cmsUserService;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] string login)
        {
            try
            {
                UserServiceModel user = _cmsUserService.GetByLogin(login);
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

    }
}